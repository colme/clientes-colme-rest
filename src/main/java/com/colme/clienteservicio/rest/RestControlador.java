package com.colme.clienteservicio.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.colme.clienteservicio.SoapClient;
import com.colme.clienteservicio.excepciones.ApiError;
import com.colme.clienteservicio.excepciones.ValidarExcepciones;
import com.colme.clienteservicio.modelo.Cliente;
import com.colme.clienteservicio.soap.ws.ClienteSOAP;
import com.colme.clienteservicio.soap.ws.GetClienteIdRequest;
import com.colme.clienteservicio.soap.ws.GetListarClienteIdRequest;
import com.colme.clienteservicio.soap.ws.ListaClientes;
import com.colme.clienteservicio.utils.MapearClientes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/clientescolme")
@Api(tags = "Clientes COLME")
public class RestControlador {
	
	@Autowired
	SoapClient soapClient;
	
	@PostMapping
	@ApiOperation(value = "Crear Cliente", notes = "Crear nuevo cliente para la empresa COLME")
	@ApiResponses(value = {@ApiResponse(code =201, message = "Cliente creado correctamente"),
			@ApiResponse(code =400, message = "Solicitud invalida")})
	public ResponseEntity<Object> crearCliente(@RequestBody @Valid Cliente cliente) {
		ApiError validacion = ValidarExcepciones.validarMayorEdad(cliente.getFechaNacimiento());
		if(validacion == null) {
			Cliente clienteResultado = MapearClientes.mapearCliente((ClienteSOAP) 
					soapClient.callWebService(MapearClientes.mapearClienteRequest(cliente)));
			return ResponseEntity.ok(clienteResultado);
		}else {
			return new ResponseEntity<Object>(validacion, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}

	@RequestMapping("/{id}")
	@ApiOperation(value = "Cliente por ID", notes = "Consulta un cliente por su ID")
	@ApiResponses(value = {@ApiResponse(code =201, message = "Cliente encontrado"),
			@ApiResponse(code =404, message = "Cliente no encontrado")})
	public ResponseEntity<Object> clienteById(@PathVariable Long id) {
		GetClienteIdRequest request = new GetClienteIdRequest();
		request.setIdCliente(id);
		Cliente cliente = new Cliente();	
		ClienteSOAP clienteSOAP = (ClienteSOAP) soapClient.callWebService(request);
		if(clienteSOAP!=null) {
			cliente = MapearClientes.mapearCliente(clienteSOAP);
			return new ResponseEntity<Object>(cliente, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Cliente no encontrado", HttpStatus.NOT_FOUND);
		}
	}
		
	@GetMapping
	@ApiOperation(value = "Lista Clientes", notes = "Consulta todos los clientes de la empresa COLME")
	@ApiResponses(value = {@ApiResponse(code =201, message = "Clientes encontrado"),
			@ApiResponse(code =404, message = "Clientes no encontrado")})
	public ResponseEntity<Object> listaClientes() {
		ListaClientes listaClientesSOAP= (ListaClientes) soapClient.callWebService(new GetListarClienteIdRequest());
		if(listaClientesSOAP!=null) {
			List<Cliente> listaClientes = MapearClientes.mapearListaCliente(listaClientesSOAP);
			return new ResponseEntity<Object>(listaClientes, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Sin registro de clientes para la empresa COLME", HttpStatus.NOT_FOUND);
		}
	}
}
