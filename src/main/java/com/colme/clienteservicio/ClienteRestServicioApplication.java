package com.colme.clienteservicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClienteRestServicioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienteRestServicioApplication.class, args);
	}

}
