package com.colme.clienteservicio;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Service
@Component("clientesColme")
public class SoapClient extends WebServiceGatewaySupport {
	
	private String endpoint = "http://localhost:8080/ws/clienteColme";

	public Object callWebService(Object request){
        return getWebServiceTemplate().marshalSendAndReceive(endpoint, request);
    }
}
