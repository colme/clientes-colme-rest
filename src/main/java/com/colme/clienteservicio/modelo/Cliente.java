package com.colme.clienteservicio.modelo;

import java.time.LocalDate;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Cliente {

	private long idCliente;

	@NotEmpty(message = "nombres: Campo requerido para el registro del cliente")
	private String nombres;
	
	@NotEmpty(message = "apellidos: Campo requerido para el registro del cliente")
	private String apellidos;
	
	@NotEmpty(message = "tipoDocumento: Campo requerido para el registro del cliente")
	private String tipoDocumento;
	
	@NotEmpty(message = "numeroDocumento: Campo requerido para el registro del cliente")
	private String numeroDocumento;

	@NotNull(message = "fechaNacimiento: Campo requerido para el registro del cliente")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd") 
	private LocalDate fechaNacimiento;

	@NotNull(message = "fechaVinculacion: Campo requerido para el registro del cliente")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd") 
	private LocalDate fechaVinculacion;
	
	@NotEmpty(message = "cargo: Campo requerido para el registro del cliente")
	private String cargo;
	
	@Min(value = 1, message = "salario: Campo requerido para el registro del cliente")
	private double salario;
	
	private String tiempoVinculacion;
	
	private String edadCliente;

	public Cliente() {

	}

	public Cliente(long idCliente, String nombres, String apellidos, String tipoDocumento, String numeroDocumento,
			LocalDate fechaNacimiento, LocalDate fechaVinculacion, String cargo, double salario) {
		super();
		this.idCliente = idCliente;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.fechaNacimiento = fechaNacimiento;
		this.fechaVinculacion = fechaVinculacion;
		this.cargo = cargo;
		this.salario = salario;
	}

	public long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public LocalDate getFechaVinculacion() {
		return fechaVinculacion;
	}

	public void setFechaVinculacion(LocalDate fechaVinculacion) {
		this.fechaVinculacion = fechaVinculacion;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public String getTiempoVinculacion() {
		return tiempoVinculacion;
	}

	public void setTiempoVinculacion(String tiempoVinculacion) {
		this.tiempoVinculacion = tiempoVinculacion;
	}

	public String getEdadCliente() {
		return edadCliente;
	}

	public void setEdadCliente(String edadCliente) {
		this.edadCliente = edadCliente;
	}
}
