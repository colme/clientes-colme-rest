package com.colme.clienteservicio.excepciones;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class ValidarExcepciones {

	public static ApiError validarMayorEdad(LocalDate fechaNacimiento) {
		LocalDate hoy = LocalDate.now();
		long edad = ChronoUnit.YEARS.between(fechaNacimiento, hoy);
		if(edad > 18) {
			return null;
		}else {
			List<String> details = new ArrayList<>();
	        details.add("El cliente no es mayor de edad, su edad es de "+edad+" años");
	        ApiError error = new ApiError("Record Not Found", details);
			return error;
		}
	}
}
