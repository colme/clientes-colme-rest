package com.colme.clienteservicio.utils;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.colme.clienteservicio.modelo.Cliente;
import com.colme.clienteservicio.soap.ws.ClienteSOAP;
import com.colme.clienteservicio.soap.ws.GetCrearClienteRequest;
import com.colme.clienteservicio.soap.ws.ListaClientes;

public class MapearClientes {

	private final static String VINCULACION = "Tiempo de Vinculación a la compañía";
	private final static String EDAD = "Edad actual del cliente";

	public static GetCrearClienteRequest mapearClienteRequest(Cliente cliente) {
		GetCrearClienteRequest clienteSOAP = new GetCrearClienteRequest();
		clienteSOAP.setNombres(cliente.getNombres());
		clienteSOAP.setApellidos(cliente.getApellidos());
		clienteSOAP.setTipoDocumento(cliente.getTipoDocumento());
		clienteSOAP.setNumeroDocumento(cliente.getNumeroDocumento());
		clienteSOAP.setFechaNacimiento(MapearDatosUtil.modificarFechaToString(cliente.getFechaNacimiento()));
		clienteSOAP.setFechaVinculacion(MapearDatosUtil.modificarFechaToString(cliente.getFechaVinculacion()));
		clienteSOAP.setCargo(cliente.getCargo());
		clienteSOAP.setSalario(cliente.getSalario());
		return clienteSOAP;
	}

	public static Cliente mapearCliente(ClienteSOAP clienteResponse) {
		Cliente cliente = new Cliente(
				clienteResponse.getIdCliente(), 
				clienteResponse.getNombres(),
				clienteResponse.getApellidos(), 
				clienteResponse.getTipoDocumento(),
				clienteResponse.getNumeroDocumento(),
				MapearDatosUtil.modificarFechaToDate(clienteResponse.getFechaNacimiento()),
				MapearDatosUtil.modificarFechaToDate(clienteResponse.getFechaVinculacion()),
				clienteResponse.getCargo(), 
				clienteResponse.getSalario()
				);
		cliente.setTiempoVinculacion(tiempo(VINCULACION, cliente.getFechaVinculacion()));
		cliente.setEdadCliente(tiempo(EDAD, cliente.getFechaNacimiento()));
		return cliente;
	}
	
	public static List<Cliente> mapearListaCliente(ListaClientes listaClientesSOAP) {
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		for (ClienteSOAP ClienteSOAP : listaClientesSOAP.getClienteSOAP()) {
			listaClientes.add(mapearCliente(ClienteSOAP));
		}
		return listaClientes;
	}

	public static String tiempo(String mensaje, LocalDate fechaInicial) {
		LocalDate fechaFinal = LocalDate.now(); 
		LocalDate tempDateTime = LocalDate.from( fechaInicial ); 
		long years = tempDateTime.until( fechaFinal, ChronoUnit.YEARS); 
		tempDateTime = tempDateTime.plusYears( years ); 
		long months = tempDateTime.until( fechaFinal, ChronoUnit.MONTHS); 
		tempDateTime = tempDateTime.plusMonths( months ); 
		long days = tempDateTime.until( fechaFinal, ChronoUnit.DAYS); 
		tempDateTime = tempDateTime.plusDays( days ); 
		return mensaje+"("+years + " años " + months + " meses " + days + " dias "+")";
	}
}
